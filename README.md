# egui-nord


## Usage:

`use egui_nord;`

`ctx.set_style(egui_nord::style());`

## Demo:

![egui-themer window with nord theme](https://gitlab.com/Abnormal_Coffee/egui-nord/-/raw/main/demo.png "Demo")
